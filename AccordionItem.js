import React from 'react';
import PropTypes from 'prop-types';

// import Heading from 'heading';
import ContentExpander from 'contentexpander';
// import Content from 'content';

import Icon from 'icon';
import IconArrow from 'icon/cheveron-down.svg';

import {getModifiers} from 'libs/component';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './AccordionItem.scss';

class AccordionItem extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'accordion-item';

		this.title = props.title;
		this.titleOpen = props.titleOpen ? props.titleOpen : this.title;

		this.state = {
			isOpen: props.isOpen,
			title: props.isOpen ? this.titleOpen : this.title
		};
	}

	open() {
		this.setState({
			isOpen: true,
			title: this.titleOpen
		});
	}

	close() {
		this.setState({
			isOpen: false,
			title: this.title
		});
	}

	componentWillReceiveProps(nextProps) {
		/*const {isOpen} = this.state;

		if (nextProps.isOpen && isOpen === false) {
			this.open();
		} else if (nextProps.isOpen === false && isOpen) {
			this.close();
		} else {
			this.setState(nextProps);
		}*/
	}

	toggle() {
		this._toggle();

		const {onToggle} = this.props;

		if (onToggle) {
			onToggle(this);
		}
	}

	componentDidMount() {
		const {open} = this.state;
		const {onToggle} = this.props;

		if (open && onToggle) {
			onToggle(this);
		}
	}

	_toggle() {
		const {isOpen} = this.state;
		if (isOpen) {
			this.close();
		} else {
			this.open();
		}
	}

	onClick = () => {
		this.toggle();
	};

	render() {
		const {children} = this.props;
		const {isOpen, title} = this.state;

		const content = typeof children === 'string' ? <Content content={children} /> : children;

		return (
			<div className={getModifiers(this.baseClass, [isOpen ? ' active' : null])}>
				<div className={`${this.baseClass}__head`} onClick={this.onClick}>
					<Heading className={`${this.baseClass}__title`} h={4} content={title} />
					<Icon className={`${this.baseClass}__arrow`} icon={IconArrow} />
				</div>
				<ContentExpander isOpen={isOpen} ref={expander => (this.expander = expander)}>
					<div className={`${this.baseClass}__body`}>{content}</div>
				</ContentExpander>
			</div>
		);
	}
}

AccordionItem.defaultProps = {
	title: null,
	titleOpen: '',
	isOpen: false,
	children: '',
	onToggle: null
};

AccordionItem.propTypes = {
	title: PropTypes.string.isRequired,
	titleOpen: PropTypes.string,
	isOpen: PropTypes.bool,
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.string, PropTypes.object]),
	onToggle: PropTypes.func
};

export default AccordionItem;
