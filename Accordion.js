import React from 'react';
import PropTypes from 'prop-types';

import AccordionItem from './AccordionItem';

import './Accordion.scss';

/**
 * Accordion
 * @description [description]
 * @example
  <div id="Accordion"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Accordion, {
	}), document.getElementById("Accordion"));
  </script>
 */
class Accordion extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'accordion';

		this.state = {
			selected: null,
			items: props.items
		};

		this.toggle = props.doToggle ? this._toggleAccordions.bind(this) : null;
	}

	componentWillReceiveProps(nextProps) {
		this.setState({items: nextProps.items});
	}

	_toggleAccordions(ref) {
		if (this.state.selected && this.state.selected.state.isOpen) {
			this.state.selected.close();
		}

		this.setState({
			selected: ref
		});
	}

	render() {
		// generate a list of accordions
		const items = this.state.items.map((item, i) => {
			return (
				<AccordionItem {...item} key={i} index={i} onToggle={this.toggle}>
					{item.children}
				</AccordionItem>
			);
		});

		return <div className={this.baseClass}>{items}</div>;
	}
}

Accordion.defaultProps = {
	doToggle: true,
	items: []
};

Accordion.propTypes = {
	doToggle: PropTypes.bool,
	items: PropTypes.array
};

export default Accordion;
